import React, { Component } from "react";

export default class TableShoe extends Component {
  decrease = -1;
  increase = 1;
  renderTable = () => {
    return this.props.gioHang.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price}</td>
          <td>
            <button
              className="btn btn-secondary"
              onClick={() => {
                this.props.handleChangeQuantity(item.id, -1);
              }}
            >
              -
            </button>
            <span className="mx-2">{item.quantity}</span>
            <button
              className="btn btn-warning"
              onClick={() => {
                this.props.handleChangeQuantity(item.id, 1);
              }}
            >
              +
            </button>
          </td>
          <button
            className="btn btn-danger"
            onClick={() => {
              this.props.handleRemoveToCart(item.id);
            }}
          >
            Xóa
          </button>
        </tr>
      );
    });
  };

  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Price</th>
              <th>Quantity</th>
              <th>Thao tác</th>
            </tr>
          </thead>
          <tbody>{this.renderTable()}</tbody>
        </table>
      </div>
    );
  }
}
