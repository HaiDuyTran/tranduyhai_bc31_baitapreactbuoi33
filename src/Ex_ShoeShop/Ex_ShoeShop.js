import React, { Component } from "react";
import { shoeArr } from "./DataShoe";
import ItemShoe from "./ItemShoe";
import TableShoe from "./TableShoe";

export default class Ex_ShoeShop extends Component {
  state = {
    shoeArr: shoeArr,
    gioHang: [],
  };
  renderListShoe = () => {
    return this.state.shoeArr.map((item) => {
      return (
        <ItemShoe
          key={item.id}
          shoeData={item}
          handleAddToCart={this.handleAddToCart}
        />
      );
    });
  };
  handleAddToCart = (shoe) => {
    // Dùng findIndex để xem trong giỏ hàng đã có sản phẩm chưa
    let index = this.state.gioHang.findIndex((item) => {
      return item.id === shoe.id;
    });
    // Th1: Chưa có sản phẩm trong cửa hàng => thêm key soLuong vào item
    // và để soLuong có value là 1
    // và thêm item vào gioHang và set lại gioHang
    let cloneGioHang = [...this.state.gioHang];
    if (index === -1) {
      let newSp = { ...shoe, quantity: 1 };
      cloneGioHang.push(newSp);
      this.setState({
        gioHang: cloneGioHang,
      });
    }
    // TH2: Đã có sản phẩm trong cửa hàng => tăng 1 giá trị của key soLuong
    // đã thêm vào item từ TH1 và set lại gioHang
    else {
      console.log(index);
      cloneGioHang[index].quantity++;
      this.setState({
        gioHang: cloneGioHang,
      });
    }
  };
  handleRemoveToCart = (idShoe) => {
    // Dùng findIndex để tìm ra index của sản phẩm trong mảng gioHang
    let index = this.state.gioHang.findIndex((item) => {
      return item.id === idShoe;
    });
    // Tìm thấy khi index!=-1 thì dùng splice để loại bỏ item trong mảng theo index
    if (index !== -1) {
      let cloneGioHang = [...this.state.gioHang];
      cloneGioHang.splice(index, 1);
      this.setState({
        gioHang: cloneGioHang,
      });
    }
  };
  handleChangeQuantity = (idShoe, soLuong) => {
    let index = this.state.gioHang.findIndex((item) => {
      return item.id === idShoe;
    });
    let cloneGioHang = [...this.state.gioHang];
    if (index !== -1) {
      cloneGioHang[index].quantity = cloneGioHang[index].quantity + soLuong;
    }
    if (cloneGioHang[index].quantity < 1) {
      cloneGioHang.splice(index, 1);
    }
    console.log(cloneGioHang);
    this.setState({
      gioHang: cloneGioHang,
    });
  };
  render() {
    return (
      <div className="container py-5">
        <TableShoe
          gioHang={this.state.gioHang}
          handleRemoveToCart={this.handleRemoveToCart}
          handleChangeQuantity={this.handleChangeQuantity}
        />
        <div className="row">{this.renderListShoe()}</div>
      </div>
    );
  }
}
